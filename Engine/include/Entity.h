#ifndef ENTITY_H
#define ENTITY_H

#include "Components/Component.h"
#include <list>
#include <string>

using namespace std;

namespace Engine
{
	class Component;
	
	class Entity
	{
		private:
			Entity();
			~Entity();
		protected:
			std::list<Component*> components;
		public:
			static Entity* CreateEntity();
			
			template<typename T>
			T* Add()
			{
				T* comp = new T();
				comp->owner = this;
				components.push_back(comp);
				return comp;
			}
			
			Component* Add(string compName);
			
			void Remove(Component* newComponent);
			    
			template<class T>
			T* GetComponent()
			{
				for (std::list<Component*>::iterator it = components.begin(); it != components.end(); ++it)
				{
					T* resultComp = dynamic_cast<T*>(*it);
					if(resultComp)
					{
						return resultComp;
					}
				}
				
				return 0;
			}
			
			Component* GetComponent(std::string compName);

            void tick(float msecs);
	};
}

#endif
