#ifndef LOGICSYSTEM_H
#define LOGICSYSTEM_H

#include <list>

namespace Engine
{
	class Entity;
	class LogicSystem
	{
     private:
            /*
              Private constructor for singleton.
            */            
            LogicSystem();
            
            /*
              Private Destructor for singleton.
            */
            ~LogicSystem();
            
     protected:
                      
            /*
                Singleton.
            */
            static LogicSystem* _instance;
    
            /*
            Second stage of initialization of the object. To make some 
            self object init instead of static initialization.
    
            @return true if everything went ok.
            */
            bool open();
    
            /*
            Second stage of destruction of the object. It releases the resources of
            the object, the static resourced must be released on Release().
            
            Inverse to open().
            */
            void close();
            
			std::list<Entity*> entities;
			
     public:
     	
	 		void AddEntity(Entity* newEntity);
     	 	
            /*
             Updates all servers.
            */
            void tick(float msecs);
            
            
            /*
              @return a single instance of this class. Patron Singleton
            */
            static LogicSystem* getSingletonPtr() {return _instance;}
            
            /*
              Initialize the instance.
              @return false if something went wrong.
            */
            static bool Init();
    
            /*
              Releases the resources.
              Inverse to Init();
            */
            static void Release();
	};
}

#endif
