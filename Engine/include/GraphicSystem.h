#ifndef GRAPHICSYSTEM_H
#define GRAPHICSYSTEM_H

#include <windows.h>

namespace Engine
{
	class GraphicSystem
	{
     private:
            /*
              Private constructor for singleton.
            */            
            GraphicSystem();
            
            /*
              Private Destructor for singleton.
            */
            ~GraphicSystem();
            
     protected:
                      
            /*
                Singleton.
            */
            static GraphicSystem* _instance;
    
            /*
            Second stage of initialization of the object. To make some 
            self object init instead of static initialization.
    
            @return true if everything went ok.
            */
            bool open();
    
            /*
            Second stage of destruction of the object. It releases the resources of
            the object, the static resourced must be released on Release().
            
            Inverse to open().
            */
            void close();
        
     public:
            /*
             Updates all servers.
            */
            void tick(float msecs);
            
            
            /*
              @return a single instance of this class. Patron Singleton
            */
            static GraphicSystem* getSingletonPtr() {return _instance;}
            
            /*
              Initialize the instance.
              @return false if something went wrong.
            */
            static bool Init();
    
            /*
              Releases the resources.
              Inverse to Init();
            */
            static void Release();
	};
}
#endif
