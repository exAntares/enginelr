#ifndef COMPONENTFACTORY_H
#define COMPONENTFACTORY_H

#include <functional>
#include <map>
#include <string>

using namespace std;

namespace Engine
{
	class Component;
	
	class ComponentFactory
	{
		public:
			/*
	        @return a single instance of this class. Patron Singleton
	        */
	        static ComponentFactory* getSingletonPtr();
			map<string, function<Component*()>> Constructors;
	        
	    private:
			ComponentFactory();
			~ComponentFactory();
		protected:
	        /*
	            Singleton.
	        */
	        static ComponentFactory* _instance;
	};
	
	class ComponentRegisterer
	{
		public:
			ComponentRegisterer(string compName, function<Component*()> constructor)
			{
				auto factory = ComponentFactory::getSingletonPtr();
				factory->Constructors.insert({compName,constructor});
			}
	};
}

#endif
