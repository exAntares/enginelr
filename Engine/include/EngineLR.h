#ifndef ENGINELR_H
#define ENGINELR_H

namespace Engine
{
	class EngineLR
	{
     private:
            /*
              Private constructor for singleton.
            */            
            EngineLR();
            
            /*
              Private Destructor for singleton.
            */
            ~EngineLR();
            
     protected:
                      
            /*
                Singleton.
            */
            static EngineLR* _instance;
    
            /*
            Second stage of initialization of the object. To make some 
            self object init instead of static initialization.
    
            @return true if everything went ok.
            */
            bool open();
    
            /*
            Second stage of destruction of the object. It releases the resources of
            the object, the static resourced must be released on Release().
            
            Inverse to open().
            */
            void close();
        
     public:
            /*
             Updates all servers.
            */
            void tick(float msecs);
            
            void OnKeyDown(int key);
            
            void OnKeyUp(int key);
            
            /*
              @return a single instance of this class. Patron Singleton
            */
            static EngineLR* getSingletonPtr() {return _instance;}
            
            /*
              Initialize the instance.
              @return false if something went wrong.
            */
            static bool Init();
    
            /*
              Releases the resources.
              Inverse to Init();
            */
            static void Release();
            
            bool _exit;
	};
}

#endif
