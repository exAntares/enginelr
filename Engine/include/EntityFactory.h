#pragma once
// for convenience
using json = nlohmann::json;

namespace Engine
{
	class Entity;

	class EntityFactory
	{
	public:
		EntityFactory();
		~EntityFactory();

		static Entity* CreateEntity(json entity);
	};
}
