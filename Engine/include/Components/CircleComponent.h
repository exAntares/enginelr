#ifndef CIRCLECOMPONENT_H
#define CIRCLECOMPONENT_H

#include <Components/Component.h>
	
namespace Engine
{
	class CircleComponent : public Component		
	{
		INC_COMP();
		
		public:
			CircleComponent();
			~CircleComponent();
			
			float Radius;
			
		protected:
			virtual void tick(float msecs);
			virtual void Deserialize(json data);
	};
}

#endif
