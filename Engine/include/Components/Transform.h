#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <Components/Component.h>

namespace Engine
{
	class Transform : public Component
	{
		INC_COMP();
		
		public:
			Transform();
			~Transform();
			double x;
			double y;
						
			virtual void tick(float msecs);
			virtual void Deserialize(json data);
	};
}

#endif
