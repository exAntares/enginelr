#ifndef COMPONENT_H
#define COMPONENT_H

#include "ComponentFactory.h"
#include <string>
#include "json/json.hpp"

using json = nlohmann::json;

#define INC_COMP()\
		public:\
			static Engine::ComponentRegisterer* MyRegisterer; \
			virtual std::string GetName()

#define IMP_COMP(COMP_NAME)\
	Engine::ComponentRegisterer* COMP_NAME::MyRegisterer = new Engine::ComponentRegisterer(#COMP_NAME, []\
	{ \
		auto newComp = new COMP_NAME();\
		return dynamic_cast<Component*>(newComp);\
	}); \
	std::string COMP_NAME::GetName() { return #COMP_NAME; }
	
namespace Engine
{
	class Entity;
	class ComponentRegisterer;
	
	class Component
	{
		public:
			Component(){}
			~Component(){}
			Entity* owner;
		protected:
		public:
			virtual std::string GetName() { return ""; }
			virtual void tick(float msecs){}
			virtual void Deserialize(json data){}
	};
}

#endif
