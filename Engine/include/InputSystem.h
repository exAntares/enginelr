#ifndef INPUTSYSTEM_H
#define INPUTSYSTEM_H
#include <map>

namespace Engine
{
	class InputSystem
	{
     private:
            /*
              Private constructor for singleton.
            */            
            InputSystem(){}
            
            /*
              Private Destructor for singleton.
            */
            ~InputSystem(){}
            
     protected:
                      
            /*
                Singleton.
            */
            static InputSystem* _instance;
    
            /*
            Second stage of initialization of the object. To make some 
            self object init instead of static initialization.
    
            @return true if everything went ok.
            */
            bool open(){return true;}
    
            /*
            Second stage of destruction of the object. It releases the resources of
            the object, the static resourced must be released on Release().
            
            Inverse to open().
            */
            void close(){}
            
            std::map<int, bool> PressedKeys;
            
     public:            
            /*
              @return a single instance of this class. Patron Singleton
            */
            static InputSystem* getSingletonPtr() {return _instance;}
            
            /*
              Initialize the instance.
              @return false if something went wrong.
            */
            static bool Init();
    
            /*
              Releases the resources.
              Inverse to Init();
            */
            static void Release();
            
            bool IsKeyDown(int key);
            
            void OnKeyDown(int key);
            
			void OnKeyUp(int key);
	};
}

#endif
