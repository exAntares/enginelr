#include "stdafx.h"

#include "Entity.h"
#include "LogicSystem.h"
#include "Components/Component.h"
#include "ComponentFactory.h"
#include <functional>
#include <map>
#include <string>

using namespace std;

namespace Engine
{
	Entity* Entity::CreateEntity()
	{
		Entity* result = new Entity();
		LogicSystem::getSingletonPtr()->AddEntity(result);
		return result;
	}
	
	Entity::Entity()
	{
	}
	
	Entity::~Entity()
	{
	}
	
	Component* Entity::Add(string compName)
	{
		auto it = ComponentFactory::getSingletonPtr()->Constructors.find(compName);
		if (it != ComponentFactory::getSingletonPtr()->Constructors.end())
		{
			Component* newComp = (it->second)();
			newComp->owner = this;
			components.push_back(newComp);
			return newComp;
		}
		
		return 0;
	}
	
	void Entity::Remove(Component* newComponent)
	{
		components.remove(newComponent);
	}

	Component* Entity::GetComponent(std::string compName)
	{
		Component* result = nullptr;
		for (std::list<Component*>::iterator it = components.begin(); it != components.end(); ++it)
		{
			if (compName.compare((*it)->GetName()) == 0)
			{
				return (*it);
			}
		}

		return result;
	}

	void Entity::tick(float msecs)
	{
		  for (std::list<Component*>::iterator it = components.begin(); it != components.end(); ++it)
		  {
		  	(*it)->tick(msecs);
		  }
	}
}

