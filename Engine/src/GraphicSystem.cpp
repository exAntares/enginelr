#include "stdafx.h"
#include "GraphicSystem.h"
#include <gl/gl.h>
#include <iostream>
#include <cassert>
#include <math.h>

namespace Engine
{
    GraphicSystem* GraphicSystem::_instance = 0;
    
    //---------------------------------------------------

    GraphicSystem::GraphicSystem()
    {
        std::cout << "GraphicSystem Constructor" << std::endl;
    }

    //---------------------------------------------------

    GraphicSystem::~GraphicSystem()
    {
        _instance = 0;
    }

    //---------------------------------------------------
    
    bool GraphicSystem::Init()
    {
        assert(!_instance && "Second initialization of GraphicSystem not allowed!");

        _instance = new GraphicSystem();
        
        std::cout << "Init GraphicSystem instance" << std::endl;

        if (!_instance->open())
        {
            Release();
            return false;
        }

        return true;

    } // Init
    
    //--------------------------------------------------------

    void GraphicSystem::Release()
    {
        assert(_instance && "GraphicSystem not initializated!");

        if(_instance)
        {
            _instance->close();
            delete _instance;
        }

    } // Release

    //--------------------------------------------------------

    bool GraphicSystem::open()
    {
        std::cout << "Opening GraphicSystem instance" << std::endl;
        return true;
    } // open

    //--------------------------------------------------------

    void GraphicSystem::close()
    {
    } // close
    
	/*
	 * Function that handles the drawing of a circle using the triangle fan
	 * method. This will create a filled circle.
	 *
	 * Params:
	 *	x (GLFloat) - the x position of the center point of the circle
	 *	y (GLFloat) - the y position of the center point of the circle
	 *	radius (GLFloat) - the radius that the painted circle will have
	 */
	void drawFilledCircle(GLfloat x, GLfloat y, GLfloat radius){
		int i;
		int triangleAmount = 20; //# of triangles used to draw circle
		
		//GLfloat radius = 0.8f; //radius
		GLfloat twicePi = 2.0f * 3.14159;// M_PI;
		
		glBegin(GL_TRIANGLE_FAN);
			glVertex2f(x, y); // center of circle
			for(i = 0; i <= triangleAmount;i++) { 
				glVertex2f(
			            x + (radius * cos(i *  twicePi / triangleAmount)), 
				    y + (radius * sin(i * twicePi / triangleAmount))
				);
			}
		glEnd();
	}

    //---------------------------------------------------
    
    void GraphicSystem::tick(float msecs)
    {
        assert(_instance && "GraphicSystem not initializated!");
        /* OpenGL animation code goes here */

        glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
        glClear (GL_COLOR_BUFFER_BIT);

        glPushMatrix ();
        glRotatef (0.0f, 0.0f, 0.0f, 1.0f);        
        glPopMatrix ();
    }
}

