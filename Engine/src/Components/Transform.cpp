#include "stdafx.h"
#include "Components/Transform.h"

namespace Engine
{
	IMP_COMP(Transform);
  
	Transform::Transform():
		x(0), y(0)
	{
	}
	
	Transform::~Transform()
	{
	}
	
	void Transform::tick(float msecs){}
	
	void Transform::Deserialize(json data)
	{	
		x = data["x"];
		y = data["y"];
	}
}
