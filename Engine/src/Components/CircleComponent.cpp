#include "stdafx.h"
#include "Components/CircleComponent.h"
#include "Components/Transform.h"
#include "Entity.h"
#include <gl/gl.h>
#include <math.h>

namespace Engine
{	
	IMP_COMP(CircleComponent);
	
	CircleComponent::CircleComponent():
		Radius(1.0f)
	{
	}
	
	CircleComponent::~CircleComponent()
	{
	}
	
	void CircleComponent::tick(float msecs)
	{
		Transform* transform = owner->GetComponent<Transform>();
		float x = transform->x;
		float y = transform->y;
		
		int i;
		int triangleAmount = 20; //# of triangles used to draw circle
		
		//GLfloat radius = 0.8f; //radius
		GLfloat twicePi = 2.0f * 3.14159;// M_PI;
		
		glBegin(GL_TRIANGLE_FAN);
			glVertex2f(x, y); // center of circle
			for(i = 0; i <= triangleAmount;i++) { 
				glVertex2f(
			            x + (Radius * cos(i *  twicePi / triangleAmount)), 
				    y + (Radius * sin(i * twicePi / triangleAmount))
				);
			}
		glEnd();
	}
	
	void CircleComponent::Deserialize(json data)
	{
		Radius = data["Radius"];
	}
}
