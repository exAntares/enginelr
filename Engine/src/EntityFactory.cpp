#include "stdafx.h"
#include "EntityFactory.h"
#include "Entity.h"

// for convenience
using json = nlohmann::json;
using namespace std;

namespace Engine
{
	json LoadAsset(std::string path)
	{
		std::ifstream t(path);
		std::string str;

		t.seekg(0, std::ios::end);
		str.reserve(t.tellg());
		t.seekg(0, std::ios::beg);

		str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
		return json::parse(str);
	}

	Entity* EntityFactory::CreateEntity(json entity)
	{
		Entity* newEntity = nullptr;
		auto components = entity["Components"];
		bool isPrototype = false;

		if (entity["Prototype"] != nullptr)
		{
			std::string prototype = entity["Prototype"];
			auto jAsset = LoadAsset("Assets/" + prototype + ".json");;
			newEntity = CreateEntity(jAsset);
			isPrototype = true;
		}
		else 
		{
			newEntity = Entity::CreateEntity();
		}

		if (components != nullptr)
		{
			for (json::iterator itComp = components.begin(); itComp != components.end(); ++itComp)
			{
				auto compName = itComp.key();
				auto newComp = isPrototype ? newEntity->GetComponent(compName) : newEntity->Add(compName);
				if (newComp != nullptr)
				{
					newComp->Deserialize(itComp.value());
				}
			}
		}

		return newEntity;
	}

	EntityFactory::EntityFactory()
	{
	}


	EntityFactory::~EntityFactory()
	{
	}
}
