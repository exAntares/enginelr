#include "stdafx.h"

#include "InputSystem.h"
#include <iostream>
#include <cassert>

namespace Engine
{
	InputSystem* InputSystem::_instance = 0;
	
	//---------------------------------------------------
    
    bool InputSystem::Init()
    {
        assert(!_instance && "Second initialization of System not allowed!");

        _instance = new InputSystem();
        
        std::cout << "Init System instance" << std::endl;

        if (!_instance->open())
        {
            Release();
            return false;
        }

        return true;

    } // Init
    
    //--------------------------------------------------------

    void InputSystem::Release()
    {
        assert(_instance && "System not initializated!");

        if(_instance)
        {
            _instance->close();
            delete _instance;
        }

    } // Release
    
    bool InputSystem::IsKeyDown(int key)
    {
    	return PressedKeys[key];
	}
    
    void InputSystem::OnKeyDown(int key)
    {
    	PressedKeys[key] = true;
	}
    
	void InputSystem::OnKeyUp(int key)
	{
		PressedKeys[key] = false;
	}
}
