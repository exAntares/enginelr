#include "stdafx.h"

#include "LogicSystem.h"
#include <iostream>
#include <cassert>
#include <list>
#include "EntityFactory.h"
#include "Entity.h"
#include <fstream>

using namespace std;
// for convenience
using json = nlohmann::json;

namespace Engine
{
    LogicSystem* LogicSystem::_instance = 0;
    
    //---------------------------------------------------

    LogicSystem::LogicSystem()
    {
        std::cout << "System Constructor" << std::endl;
    }

    //---------------------------------------------------

    LogicSystem::~LogicSystem()
    {
        _instance = 0;
    }

    //---------------------------------------------------
    
    bool LogicSystem::Init()
    {
        assert(!_instance && "Second initialization of System not allowed!");

        _instance = new LogicSystem();
        
        std::cout << "Init GraphicSystem instance" << std::endl;

        if (!_instance->open())
        {
            Release();
            return false;
        }

        return true;

    } // Init
    
    //--------------------------------------------------------

    void LogicSystem::Release()
    {
        assert(_instance && "System not initializated!");
        

        if(_instance)
        {
            _instance->close();
            delete _instance;
        }

    } // Release

    //--------------------------------------------------------

    bool LogicSystem::open()
    {
        std::cout << "Opening System instance" << std::endl;
        
		std::ifstream t("Assets/Scene.json");
		std::string str;
		
		t.seekg(0, std::ios::end);   
		str.reserve(t.tellg());
		t.seekg(0, std::ios::beg);
		
		str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
		
		auto j3 = json::parse(str);
		//vector<vector<string>>
		auto entities = j3["SceneEntities"];
		for (json::iterator it = entities.begin(); it != entities.end(); ++it)
		{
			Entity* newEntity = EntityFactory::CreateEntity(*it);;
		}
        
        return true;
    } // open

    //--------------------------------------------------------

    void LogicSystem::close()
    {
    } // close
    
    //---------------------------------------------------
    
    void LogicSystem::tick(float msecs)
    {
        assert(_instance && "System not initializated!");
		for (std::list<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
		{
			(*it)->tick(msecs);
		}
    }
    
    void LogicSystem::AddEntity(Entity* newEntity)
    {
    	entities.push_back(newEntity);
	}
}

