#include "stdafx.h"
#include "EngineLR.h"
#include <iostream>
#include <cassert>
#include <time.h>
#include "GraphicSystem.h"
#include "LogicSystem.h"
#include "InputSystem.h"

namespace Engine
{
    EngineLR* EngineLR::_instance = 0;
    
    //---------------------------------------------------

    EngineLR::EngineLR():
        _exit(false)
    {
        std::cout << "EngineLR Constructor" << std::endl;
        _instance = this;
    }

    //---------------------------------------------------

    EngineLR::~EngineLR()
    {
        _instance = 0;
    }

    //---------------------------------------------------
    
    bool EngineLR::Init()
    {
        assert(!_instance && "Second initialization of GameManager not allowed!");

        new EngineLR();
        
        std::cout << "Init GameManager instance" << std::endl;

        if (!_instance->open())
        {
            Release();
            return false;
        }

        return true;

    } // Init
    
    //--------------------------------------------------------

    void EngineLR::Release()
    {
        assert(_instance && "GameManager not initializated!");
        

        if(_instance)
        {
            _instance->close();
            delete _instance;
        }

    } // Release

    //--------------------------------------------------------

    bool EngineLR::open()
    {
        std::cout << "Opening GameManager instance" << std::endl;
        
        //initialize random seed
        srand ( time(NULL) );
		
        InputSystem::Init();
		GraphicSystem::Init();
		LogicSystem::Init();
        
        return true;
    } // open

    //--------------------------------------------------------

    void EngineLR::close()
    {
    	InputSystem::Release();
    	GraphicSystem::Release();
		LogicSystem::Release();
    } // close
    
    //---------------------------------------------------
    
    void EngineLR::tick(float msecs)
    {
        assert(_instance && "GameManager not initializated!");
        GraphicSystem::getSingletonPtr()->tick(msecs);
        LogicSystem::getSingletonPtr()->tick(msecs);
    }
    
    void EngineLR::OnKeyDown(int key)
    {
    	InputSystem::getSingletonPtr()->OnKeyDown(key);
	}
	    
	void EngineLR::OnKeyUp(int key)
    {
    	InputSystem::getSingletonPtr()->OnKeyUp(key);
    	if(key == 0x1B)
    	{
    		_exit = true;
		}
	}
}//namespace


