#include "stdafx.h"
#include "ComponentFactory.h"

namespace Engine
{
	ComponentFactory* ComponentFactory::_instance = 0;
	
	ComponentFactory* ComponentFactory::getSingletonPtr()
	{
		if(_instance == 0)
		{
			_instance = new ComponentFactory();
		}
		
		return _instance;
	}
					
	ComponentFactory::ComponentFactory()
	{
	}
	
	ComponentFactory::~ComponentFactory()
	{
	}
}
