#ifndef PLAYERCOMPONENT_H
#define PLAYERCOMPONENT_H

#include <Components/Component.h>

class PlayerComponent : public Engine::Component
{
	INC_COMP();
	
	protected:
		virtual void tick(float msecs);
};

#endif
