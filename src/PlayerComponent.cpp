#include "stdafx.h"
#include "PlayerComponent.h"
#include "Components/Transform.h"
#include "InputSystem.h"
#include "Entity.h"

using namespace Engine;

IMP_COMP(PlayerComponent);

void PlayerComponent::tick(float msecs)
{	
	if(InputSystem::getSingletonPtr()->IsKeyDown(37)) // VK_LEFT
	{
		owner->GetComponent<Transform>()->x -= msecs*0.001f;
	}
	
	if(InputSystem::getSingletonPtr()->IsKeyDown(38)) // VK_UP
	{
		owner->GetComponent<Transform>()->y += msecs*0.001f;
	}
	
	if(InputSystem::getSingletonPtr()->IsKeyDown(39)) // VK_RIGHT
	{
		owner->GetComponent<Transform>()->x += msecs*0.001f;
	}
	
	if(InputSystem::getSingletonPtr()->IsKeyDown(40)) // VK_DOWN
	{
		owner->GetComponent<Transform>()->y -= msecs*0.001f;
	}
}
